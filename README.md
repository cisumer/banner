# banner
利用SpringBoot/SpringCloud中显示banner的功能,为项目添加一些有趣的banner图
1. 引入依赖

将banner的类型作为版本号
```
	<dependency>
		<groupId>io.github.cisumer</groupId>
  		<artifactId>banner</artifactId>
  		<version>buddha</version>
	</dependency>
```
或者使用0.1版本，则需要在配置中声明显示的banner类型
```
	<dependency>
		<groupId>io.github.cisumer</groupId>
  		<artifactId>banner</artifactId>
  		<version>0.1</version>
	</dependency>
```
在properties中配置:
```
spring.banner.location=buddha
```
或在yml中配置:
``` 
spring:
  banner:
    location: buddha
 ```
 启动时控制台将显示
 ```
 
                     _ooOoo_
                    o8888888o
                    88" . "88
                   (| ^ _ ^ |)
                   O\   =   /O
                ____/ `---' \____
              .'  \\|       |//  `.
             /  \\|||   :   |||//  \
            /  _|||||  -:-  |||||-  \
            |   | \\\   -   /// |   |
            | \_|  ''\ --- /''  |   |
            \  .-\___  `-`   ___/-. /
  /\    ."" '<  `.___\_<|>_/___.'  >'"".    /\
 /  \  | | :  `- \`.;`\ _ /`;.`/ - ` : | | /  \
/      \  \ `-.   \_ __\ /__ _/   .-` /  /     \
|   /\  `-.____`-.___\_____/___.-`____.-'  /\   |
|  /  \     /\       `=---='       /\     /  \  |
 \/    \   /  \     /\     /\     /  \   /    \/
  \       /    \   /  \   /  \   /    \       /
    \ _____________________________________ /

             需求心中过     代码佛祖留
```
2. banner类型

|banner名|内容|
|---|---|
|buddha|佛祖|
|sleepbuddha|卧佛|
|alpaca|神兽（羊驼）|
|dragon|神兽（龙）|
|lion|攻城狮|
|victor|胜利手势（V）|
|book|书|